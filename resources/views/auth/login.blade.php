@extends('layouts/auth')

@section('page-title')
    Ayopop | Login
@endsection


@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a>
                <img src="{{asset('images/ayopop.png')}}">
            </a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <form action="{{route('post-login')}}" method="POST">
                {{ csrf_field() }}
                <div class="form-group has-feedback">
                    <input type="email" class="form-control" name="email" placeholder="Email" value="{{old('email')}}">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if($errors->first('email'))
                        @foreach($errors->get('email') as $key => $value)
                            <p class="text-danger error">{{$value}}</p>
                        @endforeach
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" name="password" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if($errors->first('password'))
                        @foreach($errors->get('password') as $key => $value)
                            <p class="text-danger error">{{$value}}</p>
                        @endforeach
                    @endif
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')