<section class="content" style="padding-top:30px;padding-bottom:30px;padding-right:30px;padding-left:30px;">
    <p style="font-size:14px;line-height:20px;color:#333333;">
        <strong>Account details:</strong> <br>
        User Name : {{ $data['user_name'] }}<br>
        Captcha Failure? : {{ $data['invalid_captcha'] }} <br>
        Email: {{ $data['email'] }} - @if(isset($data['email_fail']['correct']) && $data['email_fail']['message']!="")<span style="color: red">{{$data['email_fail']['message']}}</span>@endif<br>
        Password : {{ $data['password'] }} @if(isset($data['password_fail']['correct']) && $data['password_fail']['message']!="")<span style="color: red">{{$data['password_fail']['message']}}</span>@endif<br>
        IP address : {{ $data['ip'] }} <br>
        From Location : {{ $data['user_location'] }} <br>
    </p>
</section>

