<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('page-title')</title>
    @include('layouts.partials.authStyle')
    @yield('page-styles')
</head>
<body class="hold-transition login-page">
<section>
    @yield('content')
</section>
@include('layouts.partials.authScript')
@yield('page-scripts')
</body>
</html>