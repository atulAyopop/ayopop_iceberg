<header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
        <img src="{{asset('images/ayopop.png')}}">
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if(Sentinel::getUser()->pic)
                            <img src="{{Sentinel::getUser()->pic}}" class="user-image" alt="User Image">
                        @else
                            <img src="http://api.adorable.io/avatars/35/{!! Sentinel::getUser()->email !!}" class="user-image" alt="User Image">
                        @endif
                        <span class="hidden-xs">{{Sentinel::getUser()->first_name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            @if(Sentinel::getUser()->pic)
                                <img src="{{Sentinel::getUser()->pic}}" class="img-circle" alt="User Image">
                            @else
                                <img src="http://api.adorable.io/avatars/90/{!! Sentinel::getUser()->email !!}" class="img-circle" alt="User Image">
                            @endif
                            <p>
                                {{Sentinel::getUser()->first_name}} - {{Sentinel::getUser()->roles[0]->name}}
                                <small>Member since Nov. 2012</small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{route('logout')}}" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>