<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('page-title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    @include('layouts.partials.masterStyle')
    @yield('page-styles')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    @include('layouts/partials/header')
    @include('layouts/partials/sideNavigator')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            {{--<h1>--}}
                {{--{{$section_header['title']}}--}}
                {{--<small>{{$section_header['description']}}</small>--}}
            {{--</h1>--}}
            {{--<ol class="breadcrumb">--}}
                {{--@foreach($section_header['breadcrum'] as $key=>$value)--}}
                    {{--<li>--}}
                        {{--@if($value['link']!="")--}}
                            {{--<a href="{{$value['link']}}">{{$value['title']}}</a>--}}
                        {{--@else--}}
                            {{--{{$value['title']}}--}}
                        {{--@endif--}}
                    {{--</li>--}}
                {{--@endforeach--}}
            {{--</ol>--}}
        </section>

        <!-- Main content -->
        <section class="content">
           @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @include('layouts/partials/footer')
</div>
<!-- ./wrapper -->
@include('layouts.partials.masterScript')
@yield('page-scripts')
</body>
</html>