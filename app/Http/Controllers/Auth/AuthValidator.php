<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class AuthValidator extends Controller
{
    //
    public static function loginValidator($request_data){
        $rules = [
            'email' => ['required','email','regex:/^[A-Za-z0-9\.]*@(ayopop)[.](com|id)$/'],
            'password' => 'required|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
        ];
        $messages = [
            'email.required' => "Email is required.",
            'email.regex' => "Emails with ayopop domain only.",
            'email.required' => "Password is required.",
            'email.min' => "Password should contain atleast 8 characters.",
        ];

        $validator = Validator::make($request_data,$rules,$messages);

        return $validator;
    }
}
