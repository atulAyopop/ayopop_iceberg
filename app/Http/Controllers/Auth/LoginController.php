<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PHPUnit\Runner\Exception;
use Sentinel;
use Validator;
use Activation;
use App\Http\Helpers\LoginHelper;


class LoginController extends Controller
{
    use LoginHelper;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function login()
    {
        try{
            $section_header = [
                'title' => 'Users',
                'description' => '',
                'breadcrum' => [
                    [
                        'link' => route('dashboard'),
                        'title' => 'Dashboard',
                        'is_active' => false
                    ],
                    [
                        'link' => '',
                        'title' => 'List',
                        'is_active' => false
                    ],
                ],
            ];
            // Is the user logged in?
            if (Sentinel::check()) {
                return redirect()->route('dashboard');
            }
            return view('auth/login');
        }catch(Exception $e){
            dd($e->getMessage());
        }
    }

    public function postLogin(Request $request){
        try{
            $request_data = $request->all();
            $validate = AuthValidator::loginValidator($request_data);
            if($validate->fails()){
                //if validation fails
                return redirect()->back()
                    ->withErrors($validate)
                    ->withInput();
            }else{
                $credentials['email'] = $request_data['email'];
                $credentials['password'] = $request_data['password'];
                $user = Sentinel::findByCredentials($credentials);
                if (!$user) {
                    $request_data['email'] = [
                        'correct' => false,
                        'message'=> 'Unregistered Email ID.'
                    ];
                    $this->sendLoginFailureEmail($request_data);
                    return redirect()->back()
                        ->withErrors(['email'=>['You are not register with ayopop yet. Please contact admin to get access']]);
                }
                $activated = Activation::completed($user);
                if (!$activated) {
                    $request_data['email_fail'] = [
                        'correct' => false,
                        'message'=> 'Account is not activated yet.'
                    ];
                    $this->sendLoginFailureEmail($request_data, $user);
                    return redirect()->back()->withErrors(['email' => 'Please activate your account!']);
                }
                $isAuthentcated = Sentinel::validateCredentials($user, $credentials);
                if($isAuthentcated){
                    Sentinel::login($user);
                    return redirect()->route('dashboard');
                }else{
                    $request_data['password_fail'] = [
                        'correct' => false,
                        'message'=> 'Incorrect password.'
                    ];
                    $this->sendLoginFailureEmail($request_data, $user);
                    return redirect()->back()
                        ->withErrors(['email'=>['Please enter correct credentials']]);
                }

            }
        }catch(Exception $e){
            dd($e->getMessage());
        }
    }

    /**
     * Logout page.
     *
     * @return Redirect
     */
    public function logout()
    {
        // Log the user out
        Sentinel::logout();

        // Redirect to the users page
        return redirect()->route('login');
    }
}
