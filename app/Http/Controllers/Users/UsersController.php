<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UsersController extends Controller
{
    //

    public function index(Request $request){
        try{
            $section_header = [
                'title' => 'Users',
                'description' => '',
                'breadcrum' => [
                    [
                        'link' => route('dashboard'),
                        'title' => 'Dashboard',
                        'is_active' => false
                    ],
                    [
                        'link' => '',
                        'title' => 'List',
                        'is_active' => false
                    ],
                ],
            ];
            // Grab all the users
            $requestData = $request->all();
            if (isset($requestData['withTrashed']) && $requestData['withTrashed']) {
                $users = User::join('role_users','users.id','=','role_users.user_id')->select('users.*','role_users.role_id')->withTrashed();

            } elseif (isset($requestData['onlyTrashed'])) {
                $users = User::join('role_users','users.id','=','role_users.user_id')->select('users.*','role_users.role_id')->onlyTrashed();

            }elseif (isset($requestData['blocked'])) {
                $users = User::join('role_users','users.id','=','role_users.user_id')->select('users.*','role_users.role_id')->where('is_active','=',0);

            }else{
                $users = User::join('role_users','users.id','=','role_users.user_id')->select('users.*','role_users.role_id');
            }


            if(isset($requestData['name']) && $requestData['name']){
                $users->where('first_name','like','%'.$requestData['name'].'%');
            }

            if(isset($requestData['email']) && $requestData['email']){
                $users->where('email','=',$requestData['email']);
            }

            if(isset($requestData['role']) && $requestData['role']){
                $users->where('role_users.role_id','=',$requestData['role']);
            }
            $users->where(function ($query) {
                $query->where('country','!=',"IN")
                    ->orWhereNull('country');
            });
            $users = $users->orderBy('updated_at','desc')->paginate('100');
            return view('users.index', compact('users','section_header'));
        }catch(Exception $e){
            dd($e->getMessage());
        }
    }
}
