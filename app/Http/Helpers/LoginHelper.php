<?php
namespace App\Http\Helpers;

trait LoginHelper{
    public function sendLoginFailureEmail($data, $user = ""){
        if($user){
            $data['user_name'] = $user->first_name;
        }else{
            $data['user_name'] = "";
        }
        $data['invalid_captcha'] = isset($data['invalid_captcha'])?$data['invalid_captcha']:"";
        $data['invalid_captcha'] = isset($data['invalid_captcha'])?$data['invalid_captcha']:"";
        $data['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
        $data['ip'] = $ip = $_SERVER['REMOTE_ADDR'];
        $data['user_location'] = '';
        $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));

        if(isset($details->loc)){

            $coOrdinate = explode(',', $details->loc);
            $lat = trim($coOrdinate['0']);
            $lng = trim($coOrdinate['1']);
            $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false';
            $json = @file_get_contents($url);
            $data=json_decode($json);
            $status = $data->status;
            if($status=="OK")
                $data['user_location'] = $data->results[0]->formatted_address;
        }else{
            $data['user_location'] = isset($details->city). ", ". isset($details->country);

        }

        $emailData = [
            'subject' => 'Alert! Failed Login Attempt on Panel!',
            'bcc' => 'info@ayopop.com',
            'sendTo' => 'devserver@ayopop.com',
        ];
        //dd($data);
        sendEmail('emails.auth.loginFailure', ['data' => $data], $emailData);
    }
}

?>