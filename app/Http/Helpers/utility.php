<?php
use Weblee\Mandrill\Mail;

/**
 * Function to send email Using Mandrill.
 * @param $template - HTML template to send
 * @param $templateContent - Dynamic values that needs to replace in email
 * @param $data - email information example - sendTo, BCC, subject
 * @param bool $sendToGroup - Enable it to send emails to specific group
 * @return array
 */
function sendEmail($template, $templateContent, $data, $sendToGroup = false)
{
    try {
        $monitoringEmailConfig = config('monitoringEmail');
        $thirdPartyCredentialsConfig = config('thirdPartyCredentials');
        $sendEmailBy = $monitoringEmailConfig['emailSendBy']['name'];
        //$sendEmailBy = "";

        $view = View::make($template, $templateContent);
        $contents = $view->render();
        if ($sendToGroup !== false) {
            $sendTo = isset($appConfig['loginFailureEmail']) ? $monitoringEmailConfig['loginFailureEmail'] : [];
        } elseif (is_array($data['sendTo'])) {
            $sendTo = $data['sendTo'];
        } else {
            $sendTo = array(["email" => $data['sendTo']]);
        }
        $requestData = array(
            'html' => $contents,
            "subject" => $data['subject'],
            "from_email" => isset($data['from_email'])? $data['from_email'] : $monitoringEmailConfig['fromEmail']['email'],
            "from_name" => isset($data['from_name'])? $data['from_name'] : $monitoringEmailConfig['fromEmail']['name'],
            "to" => $sendTo
        );

        if (isset($data['bcc']) && $data['bcc'] != "") {
            $requestData['bcc_address'] = $data['bcc'];
        }
        //If Attachment then add attachment
        if (isset($data['attachments'])) {
            $requestData['attachments'] = $data['attachments'];
        }

        if ($sendEmailBy == 'mandrill') {
            /**
             * Send email through Mandrill
             */
            $mandrill = new Mail($thirdPartyCredentialsConfig['Mandrill']['APIKey']);
            $status = $mandrill->messages()->send($requestData);

        } else {

            // Send Email with laravel default mail
            $status = \Mail::send($template, $templateContent, function ($m) use ($requestData) {
                $m->from($requestData['from_email'], $requestData['from_name']);
                foreach ($requestData['to'] as $email) {
                    $m->to($email['email']);
                }
                $m->subject($requestData['subject']);

                if (isset($requestData['bcc_address'])) {
                    $m->bcc($requestData['bcc_address']);
                }
                //If Attachment then add attachment
                if (isset($requestData['attachments'])) {
                    foreach ($requestData['attachments'] as $attach) {
                        $m->attach($attach['fullPath']);
                    }
                }
            });
        }

        return $status;
    } catch (\Exception $e) {
        dd($e->getMessage());
        die(exception($e));
    }
}
?>