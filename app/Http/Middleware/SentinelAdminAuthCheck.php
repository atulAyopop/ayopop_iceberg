<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class SentinelAdminAuthCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (!Sentinel::check()) {
                return redirect()->route('login')->with('error', 'You must be logged in!');
            }

            return $next($request);
        }catch(Exception $e){

        }
    }
}
