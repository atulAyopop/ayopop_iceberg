<?php namespace App;

use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Database\Eloquent\SoftDeletes;
use phpDocumentor\Reflection\Types\Boolean;

class User extends EloquentUser
{

    use softDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('remember_token');
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'email',
        'password',
        'last_name',
        'first_name',
        'permissions',
        'phone',
        'code',
        'dob',
        'pic',
        'gender',
        'phone',
        'country',
        'address',
        'zip',
        'activated',
        'facebook',
        'twitter',
        'google_plus',
        'skype',
        'flickr',
        'youtube',
        'url_token',
        'login_otp',
        'otp_expire_at',
        'otp_attempts',
        'is_active',
        'password_last_updated_at',
        'last_password',
        'second_last_password',
        'password'
    ];

}
