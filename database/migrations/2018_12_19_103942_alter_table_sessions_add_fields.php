<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSessionsAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if(Schema::hasTable('sessions')){
            if(!Schema::hasColumn('sessions','user_id')){
                Schema::table('sessions', function (Blueprint $table) {
                    $table->unsignedInteger('user_id')->nullable();
                });
            }
            if(!Schema::hasColumn('sessions','ip_address')){
                Schema::table('sessions', function (Blueprint $table) {
                    $table->string('ip_address', 45)->nullable();
                });
            }
            if(!Schema::hasColumn('sessions','user_agent')){
                Schema::table('sessions', function (Blueprint $table) {
                    $table->text('user_agent')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        if(Schema::hasTable('sessions')){
            if(Schema::hasColumn('sessions','user_id')){
                Schema::table('sessions', function (Blueprint $table) {
                    $table->dropColumn('votes');
                });
            }
            if(Schema::hasColumn('sessions','ip_address')){
                Schema::table('sessions', function (Blueprint $table) {
                    $table->dropColumn('ip_address');
                });
            }
            if(Schema::hasColumn('sessions','user_agent')){
                Schema::table('sessions', function (Blueprint $table) {
                    $table->dropColumn('user_agent');
                });
            }
        }
    }
}
